<?php

namespace Bolt\Extension\TwoKings\ImportPromptus\Console;

use Bolt\Nut\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Nut command for the ImportPromptus extension.
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class ImportPromptusCommand extends BaseCommand
{

  protected function configure()
  {
    $this
      ->setName('importpromptus:import')
      ->setDescription('Import cusussen from promptus')
      ->addOption(
        'log',
        null,
        InputOption::VALUE_NONE,
        'Show log in console.'
     )
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $messages = [];

    $results = $this->app['importpromptus']->fetchRemoteData();
    
    $message = 'Starting promptus import';
    if ($input->getOption('log')) {
      $output->writeln($message);
    }
    $this->app['logger.system']->info($message, ['event' => 'import']);

    $this->app['importpromptus']->depublishAllCursussen();

    $number_of_cursussen = 0;

    foreach($results as $cursus) {
        $message = $this->app['importpromptus']->saveCursus($cursus);

        if ($input->getOption('log')) {
          $output->writeln($message);
        }

        $number_of_cursussen++;
    }

    $message = 'Finished promptus import, ' . $number_of_cursussen . ' cursus records imported.';
    if ($input->getOption('log')) {
      $output->writeln($message);
    }
    $this->app['logger.system']->info($message, ['event' => 'import']);
  }
}
