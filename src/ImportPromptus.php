<?php

namespace Bolt\Extension\TwoKings\ImportPromptus;

use Silex\Application;
use SimpleXMLElement;
use Bolt\Storage\Entity\Content;
use \DateTime;

/**
 * ImportPromptus class to do the work for the importer extension
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class ImportPromptus
{
    private $app;
    private $config;
    private $headers;    // headers for soap request
    private $payload;    // body payload for soap request
    private $results;    // results from soap request
    private $repository; // content repository cursussen
    private $planrepo;   // content repository planningen
    private $rawrecords; // raw results as an array
    private $records;    // records from simpleXML via xpath

    /**
     * Constructor.
     *
     * Sets up the environment needed for the import
     * Mostly initializes the repositories and configuration variables
     *
     * @param \Silex\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->config = $this->app['importpromptus.config'];
        $this->repository = $this->app['storage']->getRepository($this->config['target']['contenttype']);
        $this->planrepo = $this->app['storage']->getRepository($this->config['target']['planningcontenttype']);
        $this->planchildren = [
            'a:Docenten',
            'a:EindDatumTijd',
            'a:Locatie',
            'a:StartDatumTijd',
            'a:Titel',
            'a:Onderwerp'
        ];
        $this->childXPaths = [
            'a:AVWStudiePunten1',
            'a:AVWStudiePunten2',
            'a:Doelgroep',
            'a:Niveau',
            'a:Recensie',
            'a:Thema'
        ];
    }

    /**
     * This set the headers and payload, initates a SOAP request and returns the data
     */
    public function fetchRemoteData()
    {
        ini_set('max_execution_time', (5*60)); //300 seconds = 5 minutes
        $this->setupHeaders();
        $this->setupPayload();

        $uselocal = $this->config['local']['enabled'];
        $useremote = $this->config['remote']['enabled'];
        // only try to call the remote if the configuration allows us
        if ($useremote) {
            $target = $this->config['remote']['host'];
            $this->app['logger.system']->info('Importing from remote source: ' . $target, ['event' => 'import']);
            $this->callSOAPRequest();
        } elseif ($uselocal) {
            $target = $this->config['local']['filename'];
            $this->app['logger.system']->info('Importing from local source: ' . $target, ['event' => 'import']);
            $this->loadLocalExample();
        } else {
            $this->app['logger.system']->error('No available source to import.', ['event' => 'import']);
            return false;
        }

        $this->parseXMLSoup();

        return $this->records;
    }

    /**
     * Set the headers for the SOAP request
     */
    private function setupHeaders()
    {
        $this->headers = $this->config['remote']['headers'];
    }

    /**
     * Set the body payload for the SOAP request
     */
    private function setupPayload()
    {
        $this->payload = $this->app['twig']->render(
            $this->config['remote']['payloadtemplate'],
            $this->config['remote']
        );
    }

    /**
     * Perform the soap request
     */
    private function callSOAPRequest()
    {
        $guzzle = $this->app['guzzle.client'];
        $url = $this->config['remote']['host'] . $this->config['remote']['uri'];

        $options['headers'] = $this->headers;
        $options['body'] = $this->payload;

        try {
            $this->results = $guzzle->post($url, $options)->getBody()->getContents();
        } catch (\Exception $e) {
            $this->errormessage = 'Error occurred during fetch of remote import source: ' . $e->getMessage();
            $this->app['logger.system']->error($this->errormessage, ['event' => 'import']);
            // return something empty
            $this->results = false;
        }
    }

    /**
     * Temporary local helper to prevent from uselessly hammering the remote during testing
     */
    private function loadLocalExample()
    {
        $filename = $this->config['local']['filename'];
        $fullfile = dirname(getcwd()) . '/extensions/vendor/twokings/importpromptus/' . $filename;
        if($contents = file_get_contents($fullfile)) {
            $this->results = $contents;
        }
    }

    /**
     * Make xmlsoup into a real XML structure
     */
    private function parseXMLSoup()
    {
        // dump($this->results);
        $xmlelements = new SimpleXMLElement($this->results);
        $namespaces = $xmlelements->getDocNamespaces(true);
        foreach($namespaces as $key => $ns) {
            if($key === '') {
                unset($namespaces[$key]);
            } else {
                $xmlelements->registerXPathNamespace($key, $ns);
            }
        }
        //dump($xmlelements, $namespaces);
        $hierarchy = $this->config['xmlstructure']['record']['hierarchy'];
        //dump($hierarchy);
        // dump('$xmlelements', $xmlelements);
        $this->rawrecords = $xmlelements->xpath($hierarchy);
        // dump('$this->rawrecords', $this->rawrecords);
        foreach($this->rawrecords as $rawrecord) {
            $this->records[] = $this->parseNiceRecord($rawrecord, $xmlelements, $namespaces);
        }
        //dump($this->records);
    }

    /**
     * Transform the SimpleXMLSElement into a structure that bolt storage can handle
     *
     * @param $rawrecord
     * @param $xmlelements
     * @param $namespaces
     *
     * @return mixed
     */
    private function parseNiceRecord($rawrecord, $xmlelements, $namespaces) {
        $elementname = $rawrecord->getName();
        if (isset($element)) {
            unset($element);
        }
        unset($planningSXE);
        unset($childSXE);

        $element = new \stdClass();

        foreach ($rawrecord->children() as $child) {
            $name = $child->getName();
            $element->$name = $rawrecord->$name;
        }

        $rawrecord->registerXPathNamespace('a', $namespaces['a']);
        foreach ($this->childXPaths as $xPathKey) {
            $childSXE = $rawrecord->xpath($xPathKey);
            $child = reset($childSXE);
            $name = $child->getName();
            $element->$name = $child->__toString();
        }
        $planningen = $rawrecord->xpath('a:Planning/a:AvwERegister2Planning');

        $i = 0;
        foreach ($planningen as $planningSXE) {
            //$planningSXE->registerXPathNamespace('a', $namespaces['a']);
            // $name = $planningSXE->getName();
            $planning = new \stdClass();
            foreach($this->planchildren as $key => $value) {
                $childSXE = $planningSXE->xpath($value);
                //dump($key, $value, $childSXE);
                $child = reset($childSXE);
                $name = $child->getName();
                $planning->$name = $child->__toString();
            }
            $element->planningen[$i] = $planning;
            $i++;
        }
        // dump($element);
        $nicerecord = json_decode(json_encode($element), TRUE);
        //dump($nicerecord);
        // change empty objects, arrays and values into empty strings
        foreach ($nicerecord as $key => $value) {
            // only expect one level of nesting
            if (is_array($value)) {
                if (
                    count($value) === 1
                    && $key !== 'Opleiding'
                    && $key !== 'planningen'
                ) {
                    $nicerecord[$key] = reset($value);
                } else {
                    foreach ($value as $sk => $sv) {
                        if (empty($sv)) {
                            $nicerecord[$key][$sk] = '';
                        }
                    }
                }
            }
            if (empty($value)) {
                $nicerecord[$key] = '';
            }
        }
        $htmlfields = ['Inhoud', 'Leerdoelen', 'Recensie'];
        $plainfields = ['Onderwerp', 'Doelgroep', 'Thema'];
        foreach ($htmlfields as $key) {
            if (!empty($nicerecord[$key])) {
                $nicerecord[$key] = $this->cleanHtml($nicerecord[$key]);
            }
        }
        foreach ($plainfields as $key) {
          if (!empty($nicerecord[$key])) {
            $nicerecord[$key] = strip_tags($nicerecord[$key]);
          }
        }
        $nicerecord = json_decode(json_encode($nicerecord), TRUE);
        // dump($nicerecord);
        return $nicerecord;
    }

    /**
     * Remove HTMl cruft, same as in the old extension.. almost
     *
     * @param $html
     *
     * @return mixed|string
     */
    public function cleanHtml($html)
    {
        // alles swappen in een paragraaf, handig voro straks, zodat er minimaal een paragraaf is.
        $html = '<p>'. $html . '</p>'; // make sure there is at least one paragraph

        // Alle attributes strippen..
        // http://stackoverflow.com/questions/3026096/remove-all-attributes-from-an-html-tag
        $html = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $html);

        // <div> en </div> naar <p> en </p>..
        $html = str_replace("<div>", "<p>", str_replace("</div>", "</p>", $html));

        // spaties verminderen
        $html = str_replace("&nbsp;", " ", $html);
        $html = str_replace("  ", " ", $html);

        // striptaggen zodat alleen dingen over zijn die we willen
        $html = trim(strip_tags($html, "<p>,<br>,<ul>,<li>,<b>,<strong>,<em>,<i>"));

        // dubbel <p> strippen en lege paragrafen verwijderen
        $html = str_replace("  </p>", "</p>", $html);
        $html = str_replace(" </p>", "</p>", $html);
        $html = str_replace("<p><p>", "<p>", str_replace("</p></p>", "</p>", $html));
        $html = str_replace("<p><p>", "<p>", str_replace("</p></p>", "</p>", $html));
        $html = str_replace("<p><ul>", "<ul>", str_replace("</ul></p>", "</ul>", $html));
        $html = str_replace('<p></p>', "", $html);

        // als er alleen een lege paragraaf over is, dan willen we helemaal niks
        if ($html == "<p></p>") {
           $html = "";
        }

        return $html;
    }

    /**
     * Public wrapper for saveAllCursussen
     */
    public function depublishAllCursussen()
    {
      $tablename = $this->repository->getTableName();
      $active = $this->config['target']['active'];
      $inactive = $this->config['target']['inactive'];
      if ($active !== $inactive) {
        return $this->depublishSaveAllCursussen(
          [
            'table' => $tablename,
            'field' => 'status',
            'value' => $inactive
          ]
        );
      }
    }

    /**
     * Set a field for all records
     *
     * Used to depublish all cursussen before a new import with $this->depublishAllCursussen
     *
     * @param $newvalues
     */
    private function depublishSaveAllCursussen($newvalues)
    {
      return $this->app['db']->prepare('UPDATE ' . $newvalues['table'] . ' SET ' . $newvalues['field'] . ' = "' . $newvalues['value'] . '"')->execute();
    }

    /**
     * Public wrapper for deletePlannigen
     */
    public function depublishAllPlanningenByCursus($cursus_id)
    {
      $tablename = $this->planrepo->getTableName();
      return $this->deletePlannigen([
        'table' => $tablename,
        'field' => 'cursus_id',
        'value' => $cursus_id
      ]);
    }

    /**
     * Delete all planningen that match a condition
     *
     * Used by $this->depublishAllPlanningenByCursus
     *
     * @param $newvalues
     */
    private function deletePlannigen($newvalues)
    {
      return $this->app['db']->prepare('DELETE FROM ' . $newvalues['table'] . ' WHERE ' . $newvalues['field'] . ' = "' . $newvalues['value'] . '"')->execute();
    }

    /**
     * Public wrapper for insertCursus
     */
    public function saveCursus($cursus)
    {
        return $this->insertCursus($cursus);
    }

    /**
     * Save a cursus to the contenttype given in the config
     *
     * Cursussen with negative studiepunten are unpublished by default
     * Also saves planningen for a cursus if they exist
     *
     * @param $cursus: A cursus that has gone through $this->parseNiceRecord
     *
     * @return string (message with some status info)
     */
    private function insertCursus($cursus)
    {
        $record = $this->repository->findOneBy(['cursusid' => $cursus['CursusID']]);
        $message = 'Cursus: %s was updated (%d - %d)';

        // no record found - prepare a blank one
        if(!$record) {
            $record = new Content();
            $record->datepublish = new DateTime();
            $record->ownerid = $this->config['target']['ownerid'];
            $message = 'Cursus: %s was inserted (%d - %d)';
        }

        $cursusid = $cursus['CursusID'];
        $naam = !empty($cursus['Opleiding']['Omschrijving'])?(string)$cursus['Opleiding']['Omschrijving']:$cursus['CursusID'];

        $enddate = date("Y-m-d H:i:s", strtotime((string)$cursus['Opleiding']['EindDatumEnTijd']));
        if ($enddate < "1000-01-01 00:00:00") {
            $enddate = "0000-00-00 00:00:00";
        }

        $startdate = date("Y-m-d H:i:s", strtotime((string)$cursus['Opleiding']['StartDatumEnTijd']));
        if ($startdate < "1000-01-01 00:00:00") {
            $startdate = "0000-00-00 00:00:00";
        }
        if (!empty($cursus['AVWStudiePunten1'])) {
            $studiepunten = (string)$cursus['AVWStudiePunten1'];
        } else {
            $studiepunten = (string)$cursus['StudiePunten'];
        }

        if($studiepunten > 0) {
            $record->status = $this->config['target']['active'];
        } else {
            $record->status = $this->config['target']['inactive'];
        }


        switch($cursus['Opleiding']['CursusSoort']) {
            case "Academie voor Overheidsjuristen":
                $record->academie = 'acjur';
                break;
            case "Academie voor Wetgeving":
                $record->academie = 'acwet';
                break;
            case "Beide Academies":
                $record->academie = 'acjur acwet';
                break;
        }


        $record->naam = $naam;
        $record->slug = $this->app['slugify']->slugify($naam);
        $record->start_date = $startdate;
        $record->end_date = $enddate;
        $record->cursusid = $cursusid;
        $record->new = (!empty($cursus['Onderwerp'])?(string)$cursus['Onderwerp']:'');
        $record->pwo = $studiepunten;
        $record->body = (!empty($cursus['Inhoud'])?(string)$cursus['Inhoud']:'');
        $record->goals = (!empty($cursus['Leerdoelen'])?(string)$cursus['Leerdoelen']:'');
        $record->cost = (!empty($cursus['Opleiding']['Prijs'])?(int)$cursus['Opleiding']['Prijs']:'');
        $record->theme = (!empty($cursus['Thema'])?(string)$cursus['Thema']:'');
        $record->level = (!empty($cursus['Niveau'])?(string)$cursus['Niveau']:'');
        $record->targetaudience = (!empty($cursus['Doelgroep'])?(string)$cursus['Doelgroep']:'');
        $record->review = (!empty($cursus['Recensie'])?(string)$cursus['Recensie']:'');
        $record->dates = (!empty($cursus['Datums'])?(string)$cursus['Datums']:'');
        $record->searchname = (!empty($cursus['Opleiding']['Zoeknaam'])?(string)$cursus['Opleiding']['Zoeknaam']:'');
        $record->projectcode = (!empty($cursus['ProjectCode'])?(string)$cursus['ProjectCode']:'');
        $record->docent = (!empty($cursus['Hoofddocent'])?(string)$cursus['Hoofddocent']:'');

        $this->repository->save($record);

        if (!empty($cursus['planningen']) && count($cursus['planningen']) >= 1) {
            //$count = count($cursus['planningen']);
            // echo '<p>saving ' . $count . ' cursusplanningen for '. $record->id . '</p>';
            $this->savePlanningen($cursus, $record);
        }

        // $message = [$cursus, sprintf($message, $record->naam, $record->cursusid, $record->id)];
        $message = sprintf($message, $record->naam, $record->cursusid, $record->id);
        return $message;

    }

    /**
     * Save Related planning
     *
     * This part removes all planningen for a given cursus ($record->id) before inserting the new ones
     * It also uses $record->id, not $record->cursusid to link planningen to cursussen
     *
     * @param $cursus
     * @param $record
     */
    private function savePlanningen($cursus, $record)
    {
        // dump($cursus['planningen']); exit;
        $this->depublishAllPlanningenByCursus($record->id);
        foreach($cursus['planningen'] as $planning) {
            // echo '<p>saving cursusplanning '.$planning['StartDatumTijd'].' for '. $record->id . '</p>';
            $planrecord = new Content();
            $planrecord->datepublish = new DateTime();
            $planrecord->ownerid = $this->config['target']['ownerid'];
            $planrecord->status = $this->config['target']['active'];
            $planrecord->cursus_id = $record->id;
            $planrecord->onderwerp = $planning['Onderwerp'];
            $planrecord->slug = $this->app['slugify']->slugify($planning['Titel']);
            $startdate = date("Y-m-d H:i:s", strtotime($planning['StartDatumTijd']));
            if ($startdate < "1000-01-01 00:00:00") {
                $startdate = "0000-00-00 00:00:00";
            }
            $planrecord->start_date = $startdate;
            $enddate = date("Y-m-d H:i:s", strtotime($planning['EindDatumTijd']));
            if ($enddate < "1000-01-01 00:00:00") {
                $enddate = "0000-00-00 00:00:00";
            }
            $planrecord->end_date = $enddate;
            // $planrecord->cursus_id = $record->cursus_id;
            $planrecord->locatie = $planning['Locatie'];
            $planrecord->docent = $planning['Docenten'];

            $this->planrepo->save($planrecord);
        }
    }
}
