<?php

namespace Bolt\Extension\TwoKings\ImportPromptus;

use Bolt\Extension\SimpleExtension;
use Pimple as Container;
use Bolt\Menu\MenuEntry;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Markup;
use Bolt\Extension\TwoKings\ImportPromptus\ServiceProvider;
use Bolt\Extension\TwoKings\ImportPromptus\Console;

/**
 * ImportPromptus extension class.
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class ImportPromptusExtension extends SimpleExtension
{

    /**
     * {@inheritdoc}
     *
     * Extending the backend menu:
     *
     * You can provide new Backend sites with their own menu option and template.
     *
     * Here we will add a new route to the system and register the menu option in the backend.
     *
     * You'll find the new menu option under "Extras".
     */
    protected function registerMenuEntries()
    {
        /*
         * Define a menu entry object and register it:
         *   - Route http://example.com/bolt/extensions/importpromptus
         *   - Menu label 'Import Promptus'
         *   - Menu icon a Font Awesome small child
         *   - Required Bolt permissions 'settings'
         */
        $adminMenuEntry = (new MenuEntry('importpromptus-backend-page', 'importpromptus'))
            ->setLabel('Import Promptus')
            ->setIcon('fa:child')
            ->setPermission('extensions')
        ;

        return [$adminMenuEntry];
    }

    /**
     * {@inheritdoc}
     */
    public function getServiceProviders()
    {
        return [
            $this,
            new ServiceProvider\ImportPromptusServiceProvider($this->getConfig())
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerNutCommands(Container $container)
    {
        return [
            new Console\ImportPromptusCommand($container),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerTwigPaths()
    {
        return ['templates'];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerBackendRoutes(ControllerCollection $collection)
    {
        $collection->match('/extensions/importpromptus', [$this, 'importpromptusBackendPage']);
    }

    /**
     * Handles GET requests on /bolt/extensions/importpromptus and return a template.
     *
     * @param Request $request
     *
     * @return string
     */
    public function importpromptusBackendPage(Request $request)
    {

        $app = $this->getContainer();

        $results = $app['importpromptus']->fetchRemoteData();
        $messages = [];

        if($request->query->get('confirmed') == 'looksgood') {
            $message = 'Starting promptus import from site.';
            $app['logger.system']->info($message, ['event' => 'import']);

            $messages[] = $message;
            $number_of_cursussen = 0;
            $app['importpromptus']->depublishAllCursussen();
            foreach($results as $cursus) {
                $messages[] = $app['importpromptus']->saveCursus($cursus);
                $number_of_cursussen++;
            }

            $message = 'Finished promptus import, ' . $number_of_cursussen . ' cursus records imported.';
            $app['logger.system']->info($message, ['event' => 'import']);
            $messages[] = $message;
            $results = null;
        }

        $html = $this->renderTemplate('importpromptus-backend.twig', [
            'title' => 'Import Promptus',
            'results' => $results,
            'messages' => $messages
        ]);

        return new Markup($html, 'UTF-8');
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefaultConfig()
    {
        return [
            'remote' => [
                'host' => 'http://avw.e-register.nl',
                'uri' => '/PublicService.svc',
                'headers' => [
                    'SOAPAction' => 'set this in local config'
                ],
                'username' => 'set this in local config',
                'password' => 'set this in local config',
                'afgeleidenaam' => 'set this in local config',
                'versienummer' => '10.28.0.0',
                'testomgeving' => 'false',
                'checktestomgeving' => 'true',
                'payloadtemplate' => 'soap.xml.twig'
            ],
            'local' => [
                'enabled' => false,
                'filename' => 'set this in local config'
            ],
            'xmlstructure' => [
                'namespaces' => [
                    's' => "http://schemas.xmlsoap.org/soap/envelope/",
                    'base' => "http://e-Register.nl",
                    'a' => "http://schemas.datacontract.org/2004/07/Lead.Avw.eRegister2.PublicService.Contract",
                    'record' => "http://schemas.datacontract.org/2004/07/Lead.eRegister2.PublicService.Contract"
                ],
                'record' => [
                    'element' => 'AvwERegister2Cursus',
                    'hierarchy' => '//a:AvwERegister2Cursus'
                ],
            ],
            'target' => [
                'contenttype' => 'cursussen',
                'ownerid' => 3,
                'active' => 'published',
                'inactive' => 'held',
                'planningcontenttype' => 'planningen'
            ]
        ];
    }
}
