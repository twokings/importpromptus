<?php

namespace Bolt\Extension\TwoKings\ImportPromptus\ServiceProvider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Bolt\Extension\TwoKings\ImportPromptus;

/**
 * ZohoImport service provider.
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class ImportPromptusServiceProvider implements ServiceProviderInterface
{
    /** @var array */
    private $config;

    /**
     * Constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        $app['importpromptus'] = $app->share(
            function ($app) {
                return new ImportPromptus\ImportPromptus($app);
            }
        );
        $app['importpromptus.config'] = $app->share(
            function () {
                return $this->config;
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
    }
}
