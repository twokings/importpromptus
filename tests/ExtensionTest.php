<?php

namespace Bolt\Extension\TwoKings\ImportPromptus\Tests;

use Bolt\Tests\BoltUnitTest;
use Bolt\Extension\TwoKings\ImportPromptus\Extension;

/**
 * Ensure that the ImportPromptus extension loads correctly.
 *
 */
class ExtensionTest extends BoltUnitTest
{
    public function testExtensionRegister()
    {
        $app = $this->getApp();
        $extension = new Extension($app);
        $app['extensions']->register( $extension );
        $name = $extension->getName();
        $this->assertSame($name, 'ImportPromptus');
        $this->assertSame($extension, $app["extensions.$name"]);
    }
}
